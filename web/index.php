<?php

$config = yaml_parse_file("tombe.yaml");



echo "#!ipxe\n\n";
#echo "set menu-timeout 5000\n";
#echo "set submenu-timeout \${menu-timeout}\n";
#echo "set menu-default exit\n";


echo "\n:start\n";
echo "menu iPXE Boot Menu\n";
foreach ( $config["images"] as $img => $d ) {
    if ( array_key_exists("title",$d)) {
        if ( array_key_exists("hotkey",$d) and
             $d['hotkey'] != '' ) {
            printf ("item --key %s  %-15s %s\n",
                    $d['hotkey'], $img, $d['title']);
        } else {
            printf ("item          %-15s %s\n",
                    $img, $d['title']);
        }
    }
}
echo "choose --timeout {$config['config']['menu_timeout']}  ";
echo "--default {$config['config']['menu_default']}  ";
echo "selected || goto cancel\n";
#echo "set menu-timeout 0\n";
echo 'goto ${selected}';
echo "\n\n";


foreach ( $config["images"] as $img => $d ) {
    #echo $d;
    echo "#{$img}\n";
    if ( array_key_exists("title",$d)) {
        
        if (is_array($d['url'])) {
            $baseurl = $d['url']['default'];
        } else {
            $baseurl = $d['url'];
        }
        
        foreach ( ['path', 'path1', 'path2'] as $k ) {
            if ( array_key_exists($k,$d)) {
                #echo "found key {$d[$k]}\n"; 
                $baseurl .= "/".$d[$k];
            } else {
                break;
            }
        }

        
        echo ":{$img}\n";
        echo "kernel {$baseurl}/{$d['kernel']}";
        
        if (array_key_exists("bootargs",$d)) {
            echo " {$d['bootargs']}\n";
        } else {
            echo "\n";
        }
        
        if (is_array($d["initrd"])) {
            foreach ( $d["initrd"] as $i ) {
                echo "initrd {$baseurl}/{$i}\n";
            }
        } else {
            echo "initrd {$baseurl}/{$d['initrd']}\n";
        }
        echo "\n";
    }
}

?>
