<?php
    header('Content-type: text/plain');
    echo "#!ipxe"
?>
 
 
# Setup some basic convenience variables
set menu-timeout 5000
set submenu-timeout ${menu-timeout}
set args

# 

<?php

# ==========================================================================
# Default configuration
# ==========================================================================
# This lists all options that are available 
$mirror = array(
    "centos7" => "http://ftp.leg.uct.ac.za/pub/linux/centos/7/os/x86_64/",
    "sl6"     => "http://scientificlinux.mirror.ac.za/6x/x86_64/os",
);

$kopt = array(
    "ksnetcfg" => "",
    "console" => "",
);

# ==========================================================================
# Site-specific configuration
# ==========================================================================


$remoteip =  $_SERVER['REMOTE_ADDR'];
echo "# remote addr: $remoteip\n";

    if ( preg_match( "/137\.158\.94\.\d*/", $remoteip) or
         preg_match( "/137\.158\.234\.8\d*/", $remoteip) ) {

        $kopt["ks6net"]  = "ksdevice=eth0 ";
        $kopt["ks6net"] .= "ip=$remoteip netmask=255.255.255.0 ";
        $kopt["ks6net"] .= "gateway=137.158.234.1  dns=137.158.152.240";

        $kopt["ks7net"]  = "ip=$remoteip::137.158.234.1:255.255.255.0:";
        $kopt["ks7net"] .= "tmp.phy.uct.ac.za:enp1s0:none ";
        $kopt["ks7net"] .= " nameserver=137.158.152.240";
        
        $kopt["console"] = "console=ttyS1,9600";
        # $kopt["console"] = "text serial console=tty0 console=ttyS1,9600";

        #echo "set menu-default sl6uct\n";
        echo "set menu-default centos7uct\n";

    } else {
        echo "set menu-default exit\n";
    }



$baseurl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
$lxcons = "console=tty0 console=ttyS1,9600";

#$sl6mirror = "http://scientificlinux.mirror.ac.za/6x/x86_64/os";
#$sl6mirror = "http://mirror.switch.ch/ftp/mirror/scientificlinux/6x/x86_64/os";
#$sl6mirror = "http://ftp.leg.uct.ac.za/pub/linux/scientific/6x/x86_64/os/";


#$sl6inst = "$baseurl/sl6/vmlinuz text serial $lxcons method=$sl6mirror";
#
#$sl6netinst = "ksdevice=eth0"
#            .	" ip=".$_SERVER['REMOTE_ADDR']
#            . " netmask=255.255.255.0"
#            .	" gateway=137.158.234.1"
#            .	" dns=137.158.152.240";
#            
//$sl6inst = "$baseurl/sl6/vmlinuz text serial $lxcons method=$sl6mirror devfs=nomount";


foreach ($mirror as $k => $v) {
    echo "set mirror-$k $v\n";
}

foreach ($kopt as $k => $v) {
    echo "set kopt-$k $v\n";
}

echo "set tombeurl $baseurl\n";

?> 

              
###################### MAIN MENU ####################################


:start
menu iPXE boot menu
item --gap --             ------------------------- Rescue Systems ------------------------------
item --key t tinycore     Tiny Core Linux
item --key d freedos      FreeDOS
item --key m memtest      MemTest86
item --key g grub	  Grub 2 Shell
item --gap --             ------------------------- Installations ------------------------------
item --key s sl6          Scientific Linux 6
item         sl6uct       Scientific Linux 6 (Kickstart = UCT HEP)
item --key 7 centos7      CentOS 7
item         centos7uct   CentOS 7 (Kickstart = UCT HEP)
item --gap --             ------------------------- Advanced options -------------------------------
item --key c config       Configure iPXE settings
item         settings     Show (and edit) ToMBE settings
item         shell        Drop to iPXE shell
item         reload       Reload ToMBE
item         reboot       Reboot computer
item
item --key x exit         Exit iPXE and continue BIOS boot

choose --timeout ${menu-timeout} --default ${menu-default} selected || goto cancel
set menu-timeout 0
goto ${selected} 


:freedos
kernel <?php echo "$baseurl/memdisk" ?> || goto failed
initrd <?php echo "$baseurl/freedos11/fdboot.img" ?> || goto failed
boot || goto failed

:memtest
chain <?php echo "$baseurl/memtest.0" ?> || goto failed
goto start

:sl6
kernel text ${mirror-sl6}/images/pxeboot/vmlinuz ${kopt-console} method=${mirror-sl6} || goto failed
kernel ${mirror-sl6}/images/pxeboot/vmlinuz ${args}  || goto failed
initrd ${mirror-sl6}/images/pxeboot/initrd.img || goto failed
boot || goto failed

:sl6uct
kernel text serial ${mirror-sl6}/images/pxeboot/vmlinuz ${kopt-console} ks=${tombeurl}/sl6/ucthep.ks method=${mirror-sl6} ${kopt-ks6net}  || goto failed
initrd ${mirror-sl6}/images/pxeboot/initrd.img || goto failed
boot || goto failed

:centos7
#kernel ${mirror-centos7}/images/pxeboot/vmlinuz ${kopt-sercon} method=${mirror-centos7} || goto failed
kernel ${mirror-centos7}/images/pxeboot/vmlinuz ${kopt-console} repo=${mirror-centos7} ${args} || goto failed
initrd ${mirror-centos7}/images/pxeboot/initrd.img || goto failed
boot || goto failed

:centos7uct
#set args text ks=${tombeurl}/centos7/ucthep.ks ${kopt-ks7net}
set args text ks=${tombeurl}/centos7/ucthep.ks ${kopt-ks7net}
goto centos7

:tinycore

# default TinyCore kernel
#kernel <?php echo "$baseurl/tinycore/vmlinuz64 $lxcons\n" ?>  || goto failed

# modified TinyCore kernel with CONFIG_STRICT_DEVMEM disables for flashrom
kernel <?php echo "$baseurl/tinycore/vmlinuz64devmem $lxcons\n" ?>  || goto failed

initrd <?php echo "$baseurl/tinycore/corepure64.gz\n" ?> || goto failed
initrd <?php echo "$baseurl/tinycore/sercon.gz\n" ?> || goto failed
initrd <?php echo "$baseurl/tinycore/tcemirror.gz\n" ?> || goto failed
initrd <?php echo "$baseurl/tinycore/extensions.gz\n" ?> || goto failed
boot || goto failed

:grub
chain grub2pxe || goto failed
goto start

:shell
echo Type exit to get the back to the menu
shell
set menu-timeout 0
set submenu-timeout 0
goto start
 
:failed
echo Booting failed, dropping to shell
goto shell
 
:reboot
reboot
 
:exit
exit

:settings
echo Current settings:
echo
<?php
foreach ($mirror as $k => $v) {
    printf ("echo mirror-%-10s \${mirror-%s}\n", $k, $k);
    
}
echo "echo\n";
foreach ($kopt as $k => $v) {
    printf ("echo kopt-%-10s   \${kopt-%s}\n", $k, $k);
}
?>
echo
echo Change settings with 'set KEY VALUE'. Type 'exit' to return to main menu.
echo 
shell
set menu-timeout 0
set submenu-timeout 0
goto start

:reload
chain ${tombeurl}/ipxe.php || goto failed
                                         
:config
config
goto start
 
:back
set submenu-timeout 0
clear submenu-default
goto start


