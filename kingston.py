#!/usr/bin/env python3

from pytombe import devinfo,mount,fileman
import subprocess

target = "usb:serial=408D5C15AFB8E261090CB92B"


devinfo = devinfo.get_devinfo(target)

mount.mount(devinfo)

# fileman.install(devinfo, "sl6/vmlinuz")

fileman.install(devinfo, "boot/ipxe/ipxe.lkrn")
fileman.install(devinfo, "boot/ipxe/ucthep.ipxe")

fileman.install( devinfo, "boot/syslinux/syslinux.cfg",
                 template="cfg/syslinux-hep01.cfg.j2")

subprocess.run(["ls","-lR",devinfo['mountpoint']])

mount.umount(devinfo)
