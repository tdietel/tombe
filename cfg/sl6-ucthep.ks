#########################################################################
# Kickstart file for UCT HEP worker nodes
#########################################################################

###############################################################
# environment 
###############################################################
lang en_US
#langsupport --default=en_US
timezone Africa/Johannesburg


###############################################################
# basic installation settings
###############################################################
autostep
reboot					# reboot automatically when done
install					# instead of "upgrade"

text
#url --url http://

keyboard us

#bootloader --location=mbr


###########################################################################
#
# set root password
#    openssl passwd -1 SECRET
#
###########################################################################
auth --useshadow
rootpw --iscrypted $1$jvCsL7o8$XvaCMlo8EFJeCmjo1zNIh/ # md5 encrypted IPMI pw

###############################################################
# partitioning
###############################################################

clearpart --all	 --initlabel
part /boot   --fstype="ext4"  --size=200 --ondisk=sda
part /rescue --fstype="ext4"  --size=200 --ondisk=sdb
part pv.00 --grow --size 50000 --ondisk=sda

#part /boot   --fstype="ext4"  --size=200 --onpart=sda1 --noformat
#part /rescue --fstype="ext4"  --size=200 --onpart=sdb1 --noformat
#part pv.00 --grow --size 50000 --onpart=sda2

volgroup vg0 pv.00

#logvol swap --fstype="swap" --size=4096  --name=lv_swap --vgname=vg0
logvol /    --fstype="ext4"  --size=30000 --name=sl6 --vgname=vg0

bootloader --location=mbr --driveorder=sda


# zerombr yes
# clearpart --all --initlabel 
# part /boot --fstype ext3 --size 128 
# part / --fstype ext3 --size 4096 --grow --maxsize 8192
# part /var --fstype ext3 --size 4096 --grow --maxsize 8192 
# part swap --recommended 

###############################################################
# network configuration
###############################################################

# should be done on kernel command line

#network --bootproto=static --ip=192.168.0.254 --netmask=255.255.255.0 --device=eth0
#network --bootproto=dhcp --device=eth0
#firewall --enabled --http --ftp --port=https:tcp --port=ipp:tcp
#firewall --disabled



###############################################################
# hardware 
###############################################################
#mouse generic3ps/2
#mouse genericps/2 --emulthree   	# enable for 2 button mice
#xconfig --depth=16 --resolution=1024x768 --defaultdesktop=GNOME --startxonboot



###############################################################
#
# New in RHEL-4: SELinux
#
###############################################################
#selinux --enforcing
#selinux --permissive
selinux --disabled

##############################################################################
#		
# Pre Script - the following script runs on the newly installed
# machine, immediately after the kickstart file was retrieved, and
# before the disks are partitioned an the OS gets installed.
#
##############################################################################

%pre --log=/root/my-pre-log

mount > /tmp/mount.txt
df > /tmp/df.txt


#--------------------------------------------------------------
###############################################################
%pre --log /tmp/pre-install.log
echo "Starting Kickstart Pre-Installation..."

# mount old system
mkdir -p /mnt/oldroot
vgchange -a y vg0
mount -t ext4 /dev/mapper/vg0-sl6 /mnt/oldroot

# copy and store contents of /etc/ssh to use after installation
if [ -f /mnt/oldroot/etc/sysconfig/network ]
then
    source /mnt/oldroot/etc/sysconfig/network
fi

echo "identified hostname of previous installation: $HOSTNAME"


mkdir -p /tmp/keys
ls -lR /mnt/oldroot/etc/puppetlabs/puppet/ssl/
for i in certificate_requests certs public_keys private_keys
do
    if [ -f /mnt/oldroot/etc/puppetlabs/puppet/ssl/$i/${HOSTNAME}.pem ]
    then	
	cp /mnt/oldroot/etc/puppetlabs/puppet/ssl/$i/${HOSTNAME}.pem \
	   /tmp/keys/puppet_${i}_${HOSTNAME}.pem
    fi
done

cp -a /mnt/oldroot/etc/ssh/ssh_host*key* /tmp/keys
ls -l /tmp/keys/


echo "Creating cpio archive for things to keep"
cd /mnt/oldroot
ls  -1 /prevent/empty/arg/list/to/ls \
    etc/puppetlabs/puppet/ssl/*/${HOSTNAME}.pem \
    etc/ssh/ssh_host*key* \
    2>/dev/null \
| cpio -ov > /tmp/keep.cpio

ls -l /tmp/keep.cpio
cd /tmp

umount /mnt/oldroot
vgchange -a n vg0

##############################################################################
#
# Software/package selection
#
# Some packages that might come in handy on a fresh install. The bulk
# of the software will be handled by the configuration management
# system (puppet).
#
##############################################################################
%packages

freeipmi
git
openssh-clients




##############################################################################
#
# Post-installation Script - the following script runs on the newly
# installed machine, immediately after installation
#
##############################################################################
%post --nochroot --log=/mnt/sysimage/root/post-install-nochroot.log

echo -n "Running post-installation script WITHOUT chroot at"
date

export

# ----------------------------------------------------------------------------
# save log file of pre-installation script
#
cp -a /tmp/pre-install.log /mnt/sysimage/root/pre-install.log


# ----------------------------------------------------------------------------
# restore puppet and ssh keys
#

echo "Restoring keys"
cd /mnt/sysimage
cpio -idv < /tmp/keep.cpio

#for i in certificate_requests certs public_keys
#do
#    if [ -f /tmp/keys/puppet_${i}_${HOSTNAME}.pem ]
#    then
#	install -o root -g root -m 755 --directory \
#		/mnt/sysimage/etc/puppetlabs/puppet/ssl/$i/
#
#	install -o root -g root -m 644 \
#		/tmp/keys/puppet_${i}_${HOSTNAME}.pem \
#		/mnt/sysimage/etc/puppetlabs/puppet/ssl/$i/$HOSTNAME.pem
#    fi
#done
#
#for i in private_keys
#do
#    if [ -f /tmp/keys/puppet_${i}_${HOSTNAME}.pem ]
#    then
#	install -o root -g root -m 755 --directory \
#		/mnt/sysimage/etc/puppetlabs/puppet/ssl/$i/
#
#	install -o root -g root -m 644 \
#		/tmp/keys/puppet_${i}_${HOSTNAME}.pem \
#		/mnt/sysimage/etc/puppetlabs/puppet/ssl/$i/$HOSTNAME.pem
#    fi
#done
#
#ls -lR  /mnt/sysimage/etc/puppetlabs/puppet/ssl/
#
# ----------------------------------------------------------------------------
# restore ssh host keys
#
#echo "Restoring puppet keys"
#
#install -o root -g root -m 0755  /mnt/sysimage/etc/ssh/
#
#for i in ssh_host_key ssh_host_dsa_key ssh_host_rsa_key
#do
#    if [ -f /tmp/keys/$i ]
#    then
#	install -o root -g root -m 0600 \
#		/tmp/keys/$i /mnt/sysimage/etc/ssh/$i
#    fi
#
#    if [ -f /tmp/keys/$i.pub ]
#    then
#	install -o root -g root -m 0644 \
#		/tmp/keys/$i.pub /mnt/sysimage/etc/ssh/$i.pub
#    fi
#done
#





##############################################################################
#
# Post Script - the following script runs on the newly
# installed machine, immediately after installation
#
##############################################################################
%post --log=/root/post-install.log

echo "Running post-installation script WITH chroot"
date

export

# ----------------------------------------------------------------------------
# install ssh public keys
# ----------------------------------------------------------------------------
#
mkdir -p /root/.ssh/
cat > /root/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD2j+TwhFpe+JGiN/QXgAAm6QjgOBHdDmAZEx3/uLDz9PasbnW3uqggMII2XltDruuaS+2gF4r1vycnwtGkTDusIUk4lTA7KepObVrTV06Ep1ccxIJVmCfy8e/Mv3BphH6zAxMlqUeUHEyrIsY2GZFPbPUogT7/eJaPvYL07T3vW7dVMf4Me3YvG4swI7ECblvETx+BggR50KzUJGaJtEiMhajIW/NAqJ2j8dBTkKI0O71C/mJ2rPslB3osbsy88K7vAs9AV0N6shRG88W6hWOwiyM3MQYiMSNCMg7aLrKtWN3641EBt2EN1zS5rv59u1o3K6YneEmGVoeRQyNsekLd tom@dietel.net
EOF

# ----------------------------------------------------------------------------
# create grub menu entry for iPXE
# ----------------------------------------------------------------------------

# get ipxe.klrn boot file from hep04
curl -o /boot/ipxe.lkrn http://hep04.phy.uct.ac.za/tombe/ipxe/ipxe.lkrn

# make IPADDR available
source /etc/sysconfig/network-scripts/ifcfg-eth0

# make .ipxe config file
cat > /boot/ucthep.ipxe <<EOF
#!ipxe

set net0/ip ${IPADDR}
set net0/netmask ${NETMASK}
set net0/gateway ${GATEWAY}
set dns ${DNS1}
ifopen net0
route

chain http://hep04.phy.uct.ac.za/tombe/ipxe.php

EOF

# append iPXE contents to the grub.conf file
cat >> /boot/grub/grub.conf <<EOF

title iPXE
    root (hd0,0)
    kernel /ipxe.lkrn
    initrd /ucthep.ipxe

EOF


# ----------------------------------------------------------------------------
# Puppet
# ----------------------------------------------------------------------------

# install puppet: yum repo and agent
yum -y install https://yum.puppet.com/puppet5/puppet5-release-el-6.noarch.rpm
yum -y install puppet-agent


# create minimal puppet.conf for first run of agent 
cat >> /etc/puppetlabs/puppet/puppet.conf <<EOF
[main]
    server = hep09.phy.uct.ac.za
    reports = puppetdb
EOF

# enable service at boot time
chkconfig puppet on



##############################################################################



