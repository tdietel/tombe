
from pathlib import Path
from shutil import copyfile

import requests
import jinja2
import os

j2Env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath="."))

mirrorinfo = {
  "ScientificLinux": [
    "http://ftp.leg.uct.ac.za/pub/linux/scientific/",
    "http://scientificlinux.mirror.ac.za/"
  ]
}


fileinfo = {

  "syslinux.cfg": {
    "template": "cfg/syslinux.cfg",
  },

  "boot/sl6/vmlinuz": {
    "cache": True,
    "mirror": "ScientificLinux",
    "mirrorpath":  "/6x/x86_64/os/images/pxeboot/vmlinuz"
  },

  "boot/sl6/initrd.img": {
    "cache": True,
    "mirror": "ScientificLinux",
    "mirrorpath":  "/6x/x86_64/os/images/pxeboot/initrd.img"
  },

  "boot/sl6/netinstall.iso": {
    "cache": True,
    "mirror": "ScientificLinux",
    "mirrorpath":  "6x/x86_64/iso/SL-6-x86_64-netinstall.iso"
  },

  "boot/ipxe/ipxe.lkrn": {
    "cache": True,
    "url": '&'.join( [
      'https://rom-o-matic.eu/build.fcgi?BINARY=ipxe.lkrn',
      'BINDIR=bin',
      'REVISION=master',
      'DEBUG=',
      'EMBED.00script.ipxe=',
      'general.h/NSLOOKUP_CMD:=1',
      'general.h/PING_CMD:=1',
      'console.h/CONSOLE_SERIAL:=1',
      "serial.h/COMCONSOLE=COM2",
      "serial.h/COMSPEED=9600" ] )
  },

  "boot/ipxe/ucthep.ipxe": {
    "template": "cfg/ucthep.ipxe.j2",
  }

}

cachedir = "download"

def install (devinfo, fdata, **kwargs):

    if isinstance(fdata, str):

        if fdata in fileinfo:
            finfo = fileinfo[fdata]
        else:
            finfo = {}

        # set default value for destination
        finfo["name"] = fdata
        finfo["dest"] = Path(devinfo['mountpoint'] + "/" + fdata)

    elif isinstance(fdata, dict):
        finfo = fdata

    if kwargs is not None:

        if "dest" in kwargs:
            finfo["dest"] = Path(devinfo['mountpoint'] + "/" + kwargs["dest"])

        if "template" in kwargs:
            finfo["template"] = kwargs["template"]


    if "cache" in finfo and finfo["cache"]:
        finfo["cachefile"] = Path(cachedir + "/" + finfo["name"])
    else:
        finfo["cachefile"] = None

    print(finfo)

    if "url" in finfo:
        install_download_file(finfo)

    elif "template" in finfo:
        install_template_file(finfo)

# ---------------------------------------------------------------------------
def install_download_file(finfo):

    # If the file exists (and is up to date), we are done.
    if finfo["dest"].is_file():
        # maybe we should check file creation/mod dates, but let's keep it
        # simple for now...
        return

    # The file does not exist, and we have to create it.

    if finfo["cachefile"] is not None and not finfo["cachefile"].is_file():
        if "mirror" in finfo:
            mirror = finfo['mirror']
        else:
            mirror = None

        url = jinja2.Template(finfo['url']).render(mirror=mirror)
        print (url)
        # NOTE the stream=True parameter below
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            cache.parent.mkdir(0o755, True, True)
            with open(cache, 'wb') as f:
                for chunk in r.iter_content(chunk_size=8192):
                     # filter out keep-alive new chunks
                    if chunk:
                        f.write(chunk)
                    # f.flush()


    dest.parent.mkdir(0o755, True, True)
    copyfile(cache, dest)

# ---------------------------------------------------------------------------
def install_template_file(finfo):

    tmpl = j2Env.get_template(finfo["template"])
    with open(finfo["dest"], 'w') as outfile:
        outfile.write(tmpl.render())
