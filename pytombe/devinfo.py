#!/usr/bin/env python3

import argparse
import plistlib
import subprocess


def get_devinfo( target ):

    usbinfo = mac_get_usbinfo()

    if target[0:11] == "usb:serial=":
        usbinfo = mac_get_usbinfo()

        for i in usbinfo:
            if 'serial' not in i: continue
            if i['serial'] != target[11:]: continue

            if len(i['media']) != 1:
                print("only USB devices with 1 medium are supported")
                return {}

            if len(i['media'][0]['volumes']) != 1:
                print("only USB devices with 1 partition are supported")
                return {}

            devinfo = {
              "volname":    i['media'][0]['volumes'][0]['name'],
              "size":       i['media'][0]['volumes'][0]['size'],
              "uuid":       i['media'][0]['volumes'][0]['uuid'],
              "partdevice": i['media'][0]['volumes'][0]['device'],
              "diskdevice": i['media'][0]['device'],
            }

            devinfo = mac_update_mountinfo(devinfo)
            return devinfo

    return {}


def mac_get_usbinfo():

    res = subprocess.run( ["system_profiler", "-xml", "SPUSBDataType"],
                             capture_output=True)

    usbdata = plistlib.loads(res.stdout)

    if '_items' in usbdata[0]:
        return mac_parse_usbinfo_items(usbdata[0]['_items'][0]['_items'], [])


def mac_parse_usbinfo_items(usbdata, usbinfo):

    for i in usbdata:

        # print ('-----------------------------------------------------------')
        # print (i)

        if i['vendor_id']=="apple_vendor_id":
            vendor_id = 0x05ac
        else:
            vendor_id = int(i['vendor_id'][0:6], 16)

        usbdevinfo = {
          "usb_vendor": vendor_id,
          "usb_product": int(i['product_id'], 0)
        }

        if 'manufacturer' in i and 'serial_num' in i:
            usbdevinfo['serial'] = i["serial_num"]
            usbdevinfo['description'] = "%s %s (%s)" % (i["manufacturer"],i["_name"],i["serial_num"])
        elif 'serial_num' in i:
            usbdevinfo['serial'] = i["serial_num"]
            usbdevinfo['description'] = "%s (%s)" % (i["_name"],i["serial_num"])
        else:
            usbdevinfo['description'] = "%s" % (i["_name"])

        if 'Media' in i:
            # print ('-----------------------------------------------------------')
            # print (i)
            # print (i['Media'])
            # print (len(i['Media']))

            usbdevinfo["media"] = []

            for m in i['Media']:
                if 'bsd_name' not in m: continue
                if 'volumes' not in m: continue

                minfo = {}
                minfo["device"] = "/dev/" + i['Media'][0]['bsd_name']

                minfo["volumes"] = []


                for v in m["volumes"]:
                    vinfo = {}
                    vinfo["name"]   = v["_name"]
                    vinfo["device"] = "/dev/"+v["bsd_name"]
                    vinfo["uuid"]   = v["volume_uuid"]
                    vinfo["fstype"] = v["file_system"]
                    vinfo["size"]   = v["size_in_bytes"]

                    minfo["volumes"].append(vinfo)

                usbdevinfo["media"].append(minfo)

        usbinfo.append( usbdevinfo )

        if "_items" in i:
            usbinfo = mac_parse_usbinfo_items(i['_items'], usbinfo)

    return usbinfo

def mac_update_mountinfo(devinfo):

    res = subprocess.run( ["diskutil", "info", "-plist", devinfo["partdevice"]],
                          capture_output=True)

    mntdata = plistlib.loads(res.stdout)

    # print(mntdata)

    if 'MountPoint' in mntdata and mntdata['MountPoint'] != "":
        devinfo['mountpoint'] = mntdata['MountPoint']
    else:
        if 'mountpoint' in devinfo: del devinfo['mountpoint']

    return devinfo


def print_usbinfo(usbinfo):
    for i in usbinfo: print_usbdevinfo(i)

def print_usbdevinfo(usbdevinfo):

    print ( "%04x:%04x   %s" % (
        usbdevinfo['usb_vendor'],
        usbdevinfo['usb_product'],
        usbdevinfo['description']) )

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Mount device or image for ToMBE installation.')

    parser.add_argument('target', type=str,
    help='the ToMBE installation target to mount')

    # parser.add_argument('--mount', dest='mode', action='store_const',
    # const="mount", default="mount",
    # help='mount the device (default)')
    #
    # parser.add_argument('--umount', dest='mode', action='store_const',
    # const="umount", default="mount",
    # help='mount the device (default)')

    args = parser.parse_args()

    # print(args)

    print_usbinfo ( mac_get_usbinfo(  ) )

    print( get_devinfo(args.target) )
