#!/usr/bin/env python3

import argparse
import subprocess
from pytombe import devinfo

def mount(device, mntpoint=None):

    device = devinfo.mac_update_mountinfo(device)

    if "mountpoint" in device:
        return #device["mountpoint"]

    if mntpoint is not None:
        subprocess.run( ["diskutil", "mount", "-mountPoint", mntpoint, device['partdevice']] )
        #device["mountpoint"] = mntpoint
    else:
        subprocess.run( ["diskutil", "mount", device['partdevice']] )

    device = devinfo.mac_update_mountinfo(device)


def umount(device):

    device = devinfo.mac_update_mountinfo(device)

    if 'mountpoint' not in device: return

    subprocess.run( ["diskutil", "unmount", device['partdevice']] )

    device = devinfo.mac_update_mountinfo(device)



# #  away to avoid a function call for mount
#
# import ctypes
# import ctypes.util
# import os
# libc = ctypes.CDLL(ctypes.util.find_library('c'), use_errno=True)
# libc.mount.argtypes = (ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.c_ulong, ctypes.c_char_p)
#
# def mount(source, target, fs, options=''):
#   ret = libc.mount(source, target, fs, 0, options)
#   if ret < 0:
#     errno = ctypes.get_errno()
#     raise OSError(errno, "Error mounting {} ({}) on {} with options '{}': {}".
#      format(source, fs, target, options, os.strerror(errno)))
#
# mount('/dev/sdb1', '/mnt', 'ext4', 'rw')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Mount device or image for ToMBE installation.')

    parser.add_argument( 'target', type=str,
                         help='the ToMBE installation target to mount')
    parser.add_argument( 'mntpoint', type=str, nargs="?",
                         help='mount point')
    parser.add_argument( '--mount', dest='mode', action='store_const',
                         const="mount", default="mount",
                         help='mount the device (default)')
    parser.add_argument( "-u", '--umount', dest='mode', action='store_const',
                         const="umount", default="mount",
                         help='mount the device (default)')

    args = parser.parse_args()
    # print(args)

    device = devinfo.get_devinfo(args.target)
    # print(device)

    if args.mode == "mount":
        mount(device, args.mntpoint)

    elif args.mode == "umount":
        umount(devinfo)

    else:
        print("ERROR: unknow run mode \"%s\"" % args.mode)
