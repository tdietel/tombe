ToMBE: Tom's Multi-Boot Environment
===================================

Tom's Multi-Boot Environment (ToMBE) aims at providing a rather personal
selection of Linux installation and rescue system tools, together with helpers
for setting up ways of booting these features via:

* iPXE over http
* removable USB media
* ISO images

Overview
========

Features
--------

- USB image support
  - [X] Create raw USB images
  - [X] BIOS support
  - [ ] EFI support
  - [X] Create .vmdk file for VirtualBox
  - [X] Boot VirtualBox from image file
- Hardware USB device support
  - [X] Mount USB volumes on macOS
  - [ ] Mount USB volumes on Linux
  - [ ] Install SysLinux
  - [X] Create .vmdk file for VirtualBox
  - [ ] Boot VirtualBox from image file 
- SysLinux configuration
  - [X] Create SysLinux configuration file with erb


Vagrant
-------

The `Vagrantfile` sets up a VirtualBox virtual machine that serves as a
reference platform for ToMBE. Initially aimed at building USB images, it can
in the future also serve as a Web/DHCP/TFTP server for ToMBE.

While many tasks should run on most Unix platforms that can mount a raw disk
image (e.g. Linux, macOS...), some actions require Linux (e.g. syslinux is not
available on macOS). This VM allows to run these tasks also on a Mac.

VirtualBox Test Environment - vbox.sh
-------------------------------------

The shell script `vbox.sh` can setup, start, stop and destroy a VirtualBox VM
that can be used to test a boot image. The boot image is attached as the only
HDD in the system.
