#!/bin/sh

VMNAME="ToMBE"
#USBIMG="/Users/tom/admin/tombe/img/tombe.dmg"
#USBIMG="img/kingston.vmdk"
#USBIMG=usb/syslinux-8M.dmg
USBIMG=$(diskutil info E2EFEC2E-E224-31A8-92BA-BEB351F6FE5A | awk '/Part of Whole/ {print $4}')

RED='\033[0;31m'
GREEN='\033[0;32m'
GRAY='\033[0;37m'
BLACK='\033[0;30m'
NC='\033[0m' # No Color


status () {
  #  echo "${BLACK}Checking virtual machine \"${VMNAME}\"${GRAY}"

  STATE=$(VBoxManage showvminfo "${VMNAME}" 2>/dev/null \
  | perl -ne 'print "$1\n" if /State: *(.*) \(/;')

  [[ "$STATE" == "" ]] && STATE=unconfigured

  case $1 in
    on)
      [[ "$STATE" == "running" ]] && return 0 || return 1
      ;;
    off)
      [[ "$STATE" == "powered off" ]] && return 0 || return 1
      ;;
    uncfg)
      [[ "$STATE" == "unconfigured" ]] && return 0 || return 1
      ;;

    *)
      case "$STATE" in
      "powered off")
        echo "${BLACK}VM is ${RED}${STATE}${BLACK}"
        return 0
        ;;
      "running")
        echo "${BLACK}VM is ${GREEN}${STATE}${BLACK}"
        return 0
        ;;
      "unconfigured")
        echo "${BLACK}VM does ${RED}not exist${BLACK}"
        return 0
        ;;
      *)
        echo "${RED}Unknown state of VM \"${VMNAME}\": ${STATE}${BLACK}"
        return 1
        ;;
    esac
  esac
}

createvm () {
  echo "${BLACK}Creating virtual machine \"${VMNAME}\"${GRAY}"

  VBoxManage createvm --name "${VMNAME}" --register
  VBoxManage modifyvm "${VMNAME}" --usbxhci on
  VBoxManage storagectl "${VMNAME}" --name SATA --add sata --portcount 4

  echo ${BLACK}
  RETVAL=$?
  return $RETVAL
}

prepareimg () {

  set -v


  OLDIMG=$(VBoxManage showvminfo ToMBE | awk '/^SATA/ { print $4 }')

  echo "${BLACK}Detach old USB image${GRAY}"
  VBoxManage storageattach "${VMNAME}" \
                         --storagectl "SATA" \
                         --device 0 \
                         --port 0 \
                         --type hdd \
                         --medium none

  echo "${BLACK}Close old VirtualBox medium \"${OLDIMG}\"${GRAY}"
  VBoxManage closemedium disk ${OLDIMG}

  if echo ${USBIMG} | grep '\.dmg$'
  then
    echo "${BLACK}Converting image \"${USBIMG}\" to VMDK${GRAY}"
    VMDKIMG=$(echo ${USBIMG} | sed 's/\.dmg$/.vmdk/')
    rm -f ${VMDKIMG}
    VBoxManage convertfromraw --format vmdk ${USBIMG} ${VMDKIMG}

  elif echo ${USBIMG} | grep '^disk'
  then
    echo "${BLACK}Packing \"${USBIMG}\" to VMDK${GRAY}"
    VMDKIMG=vboximg.vmdk
    rm -f ${VMDKIMG}
    sudo chmod 666 /dev/${USBIMG}
    VBoxManage internalcommands createrawvmdk -filename ${VMDKIMG} \
  	-rawdisk /dev/${USBIMG}
  fi



  # make sure the image is up to date
  #echo "${BLACK}Updating USB image \"${USBIMG}\"${GRAY}"
  #rm -f ${USBIMG}
  #make ${USBIMG}
  #make -C $(dirname ${USBIMG}) $(basename ${USBIMG})

  echo "${BLACK}Attaching USB image \"${VMDKIMG}\" to VM \"${VMNAME}\"${GRAY}"
  VBoxManage storageattach "${VMNAME}" \
                         --storagectl "SATA" \
                         --device 0 \
                         --port 0 \
                         --type hdd \
                         --medium ${VMDKIMG}

 echo "${BLACK}Info: storage configuration for VM \"${VMNAME}\"${GRAY}"
 VBoxManage showvminfo "${VMNAME}" | grep '^Storage'
 VBoxManage showvminfo "${VMNAME}" | grep '^SATA'

}

destroyvm () {
  echo "${BLACK}Destroying virtual machine \"${VMNAME}\"${GRAY}"

  sleep 1
  VBoxManage unregistervm "${VMNAME}" --delete
  until status uncfg
  do
    echo "${BLACK}VM is still running, trying to destroy again in 3 sec...${GRAY}"
    sleep 3
    VBoxManage unregistervm "${VMNAME}" --delete
  done

  echo ${BLACK}
  RETVAL=$?
  return $RETVAL
}

startvm () {
  echo "${BLACK}Starting virtual machine \"${VMNAME}\"${GRAY}"
  VBoxManage startvm "${VMNAME}"
  RETVAL=$?
  return $RETVAL
}

stopvm () {
  echo "${BLACK}Stopping virtual machine \"${VMNAME}\"${GRAY}"
  VBoxManage controlvm "${VMNAME}" poweroff
  RETVAL=$?
  return $RETVAL
}

vmexists () {
  echo foo
}

case "$1" in
  start|up)
    status uncfg && createvm
    until status off; do sleep 1; done
    prepareimg
    status off && startvm
    ;;
  stop)
    #vmexists || createvm
    status on && stopvm
    ;;
  restart)
    status on && stopvm
    status uncfg && createvm
    until status off; do sleep 1; done
    prepareimg
    status off && startvm
    ;;
  create)
    createvm
    ;;
  destroy)
    status on && stopvm
    until status off; do echo waiting; sleep 1; done
    #VBoxManage showvminfo ${VMNAME}
    #VBoxManage list runningvms
    status off && destroyvm
    ;;
  status)
    status
    ;;
  *)
    echo $"Usage: $0 {start|up|stop|destroy|status}"
    RETVAL=2
esac

exit $RETVAL
