#!/bin/sh

MOUNTPOINT=mnt/syslinux

for i in /usr/lib/syslinux/modules/bios/ /usr/share/syslinux
do
  if [ -d $i ] ; then
    SYSLINUX_DIR=$i
    break
  fi
done


set -x

if [ -f $1 ]
then
  DISKDEVICE=$(losetup -P --find --show $1)
  PARTDEVICE=${DISKDEVICE}p1
else
  DISKDEVICE=$1
  PARTDEVICE=${DISKDEVICE}1
fi

syslinux --install ${PARTDEVICE}

parted ${DISKDEVICE} set 1 boot on
parted ${DISKDEVICE} print

dd conv=notrunc bs=440 count=1 if=${SYSLINUX_DIR}/mbr.bin of=${DISKDEVICE}

mkdir -p ${MOUNTPOINT}
mount ${PARTDEVICE} ${MOUNTPOINT}


mkdir -p ${MOUNTPOINT}/boot/syslinux
for i in linux.c32 menu.c32
do
  cp ${SYSLINUX_DIR}/$i ${MOUNTPOINT}/boot/syslinux
done

ls -lR ${MOUNTPOINT}

umount ${MOUNTPOINT}

if [ -f $1 ]
then
  losetup -d ${DISKDEVICE}
fi
