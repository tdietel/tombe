#!/bin/sh

# ---------------------------------------------------------------------
# Parse command line options
# ---------------------------------------------------------------------

# read the options
TEMP=$(getopt -o s:l: \
--long size:,label: \
-- "$@")
eval set -- "$TEMP"

echo FOO

IMGLABEL="NONAME"
IMGSIZE="16MB"

# extract options and their arguments into variables.
while true ; do
    case "$1" in
        -s|--size) IMGSIZE=$2 ; shift 2 ;;
        -l|--label) IMGLABEL=$2 ; shift ;;
        --) shift ; break ;;
        *) echo "Internal error!" ; exit 1 ;;
    esac
done

echo IMGSIZE=$IMGSIZE
echo IMGLABEL=$IMGLABEL
echo file: $1

# ---------------------------------------------------------------------
# Other variables
# ---------------------------------------------------------------------

SUDO=sudo
DD=dd
PARTED=/sbin/parted

# ---------------------------------------------------------------------
# The actual script
# ---------------------------------------------------------------------
set -x
${DD} bs=${IMGSIZE} count=1 if=/dev/zero of=$@
${PARTED} $@ mklabel msdos
${PARTED} --script $@ mkpart primary fat32 0% 100%
${PARTED} $@ set 1 boot on
${PARTED} $@ print
LOOPDEV=$(${SUDO} losetup -P -f --show $@)
${SUDO} mkfs -t vfat ${LOOPDEV}p1
${SUDO} fatlabel ${LOOPDEV}p1 ${IMGLABEL}
${SUDO} losetup -d ${LOOPDEV}
