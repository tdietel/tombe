
# change sudo mechanism if necessary (e.g. if running as root)
SUDO=sudo

SYSLINUX_DIR=/usr/share/syslinux

VOLNAME ?= TOMBE

syslinux-%.raw: syslinux.mk
	dd bs=$* count=1 if=/dev/zero of=$@
	parted $@ mklabel msdos
	parted --script $@ mkpart primary fat32 0% 100%
	parted $@ set 1 boot on
	parted $@ print
	@echo
	$(SUDO) losetup -P -f $@
	$(SUDO) mkfs -t vfat /dev/loop0p1
	$(SUDO) fatlabel /dev/loop0p1 $(VOLNAME)
	$(SUDO) syslinux --install /dev/loop0p1
	$(SUDO) mount /dev/loop0p1 /mnt/
	$(SUDO) mkdir -p /mnt/boot/syslinux
	for i in linux.c32 menu.c32; 	do \
	  $(SUDO) cp $(SYSLINUX_DIR)/$$i /mnt/boot/syslinux; \
	done
	@echo
	ls -lR /mnt
	$(SUDO) umount /mnt
	$(SUDO) losetup -d loop0
