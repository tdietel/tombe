
# ============================================================================
# some general settings
IPXEDIR=$(HOME)/admin/ipxe
IPXEMAKE=make -C $(IPXEDIR)/src CONFIG=tombe
IPXECONFIG=$(IPXEDIR)/src/config/local/tombe

THISDIR=$(shell pwd)



# ============================================================================
# top level targets

TGTS =	ipxe.lkrn \
	ipxe.efi \
	ipxe-hep04.efi \
	ipxe-ucthep.efi \
	undionly.kpxe \
	undionly-hep04.kpxe \
	undionly-ucthep.kpxe

# default target
all: $(TGTS)

# this was a test to move all generated files to bin/
#all: $(patsubst %,bin/%,$(TGTS))

clean:
	rm -f $(TGTS)


# ============================================================================
# calls to iPXE

$(IPXECONFIG): $(wildcard config/*)
	ln -sf $(THISDIR)/config $@

ipxe.lkrn: $(IPXECONFIG)
	$(IPXEMAKE) bin/ipxe.lkrn
	cp $(IPXEDIR)/src/bin/$@ $@

undionly.kpxe: $(IPXECONFIG)
	$(IPXEMAKE) CONFIG=tombe bin/$@
	cp $(IPXEDIR)/src/bin/$@ $@

undionly-%.kpxe: %.ipxe $(IPXECONFIG)
	$(IPXEMAKE) EMBED=$(THISDIR)/$*.ipxe bin/undionly.kpxe
	cp $(IPXEDIR)/src/bin/undionly.kpxe $@

ipxe.efi: $(IPXECONFIG)
	$(IPXEMAKE) bin-x86_64-efi/ipxe.efi
	cp $(IPXEDIR)/src/bin-x86_64-efi/ipxe.efi $@

ipxe-%.efi: %.ipxe $(IPXECONFIG)
	$(IPXEMAKE) EMBED=$(THISDIR)/$*.ipxe bin-x86_64-efi/ipxe.efi
	cp $(IPXEDIR)/src/bin-x86_64-efi/ipxe.efi $@

#ipxe_hep05.lkrn:
#	rm $(IPXEDIR)/src/bin/ipxe.lkrn
#	make -C $(IPXEDIR)/src CONFIG=tombe bin/ipxe.lkrn
#	cp $(IPXEDIR)/src/bin/ipxe.lkrn $@

# ============================================================================
# build other file formats, e.g. floppy, USB or ISO images

ipxe_ucthep.img: syslinux.img.gz ipxe.lkrn syslinux.cfg ucthep.ipxe
	gunzip -c $< > $@
	mcopy -i $@  ipxe/ipxe.lkrn ::ipxe.krn
	mcopy -i $@  ipxe/syslinux.cfg ::syslinux.cfg
	mcopy -i $@  ipxe/ucthep.ipxe ::ucthep.ipx



