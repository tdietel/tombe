
tombe_MOUNTPOINT=/media/TOMBE

tombe: tombe-files tombe-finish

tombe-files: $(tombe_MOUNTPOINT)/syslinux.cfg


clean::
	rm -f img/tombe.* img/.tombe.*

scan::
	@echo "   tombe       --  A default DMG"

umount: img/.tombe.umount

tombe-finish: tombe-files tombe-umount
tombe-files: tombe-mount
tombe-mount: img/tombe.dmg
tombe-umount: img/.tombe.umount


img/tombe.dmg: img/syslinux-8M.raw.gz
	gunzip -c $< > $@

$(tombe_MOUNTPOINT)/syslinux.cfg: syslinux.cfg img/.tombe.mount
	cp $< $@

img/tombe.vmdk: img/tombe.dmg
	@rm -f $@

	if VBoxManage showmediuminfo $@ >/dev/null 2>&1 ; then \
	  echo "removing medium"; VBoxManage closemedium $@ ; \
	fi

	VBoxManage convertfromraw --format vmdk $< $@


img/.tombe.mount: img/tombe.dmg
	sudo losetup -P -f img/tombe.dmg
	sudo mkdir -p $(tombe_MOUNTPOINT)
	sudo mount -o umask=0000 /dev/loop0p1 $(tombe_MOUNTPOINT)
	@touch $@
	@rm -f img/.tombe.umount

img/.tombe.umount: img/tombe.dmg
	sudo umount $(tombe_MOUNTPOINT)
	sudo losetup -D
	@touch $@
	@rm -f img/.tombe.mount
