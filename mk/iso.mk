

TOPTARGETS := all clean

SUBDIRS := 
#SUBDIRS := ipxe tinycore

$(TOPTARGETS): $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

.PHONY: $(TOPTARGETS) $(SUBDIRS)

include local.mk

# ----------------------------------------------------------------------------
# High level targets
#

# make everything
all: web iso

# create bootable CD/DVD image
iso: tombe.iso

# we want to serve this directory via http
web: syslinux/memdisk

#download: $(LOCALFILES)

clean:
	rm -f tombe.iso
	rm -rf boot/syslinux




# ----------------------------------------------------------------------------
# Mirrors for downloading
#
# These mirrors can be overwritten in local.mk

CENTOS_MIRROR   ?= http://ftp.leg.uct.ac.za/pub/linux/centos/
DEBIAN_MIRROR   ?= http://ftp.leg.uct.ac.za/debian
SYSLINUX_MIRROR ?= http://kernel.mirror.ac.za/linux/utils/boot/syslinux/

#SYSLINUX_MIRROR ?= https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux


# ----------------------------------------------------------------------------
# Download iPXE files
#
ipxe/ipxe.lkrn:
	mkdir -p $(dir $@)
	curl -o $@ 'https://rom-o-matic.eu/build.fcgi?BINARY=ipxe.lkrn&BINDIR=bin&REVISION=master&DEBUG=&EMBED.00script.ipxe=&general.h/NSLOOKUP_CMD:=1&general.h/PING_CMD:=1'


# ----------------------------------------------------------------------------
# SysLinux / IsoLinux
#

SYSLINUX_VERSION=6.03
SYSLINUX_TGZ=download/syslinux-$(SYSLINUX_VERSION).tar.gz
#SYSLINUX_BUILD=build/syslinux-$(SYSLINUX_VERSION)

SYSLINUX = $(SYSLINUX_TGZ) ;
SYSLINUX += echo SYSLINUX_$(1)_$(2) ;
SYSLINUX += echo $$(SYSLINUX_$(1)_$(2)) ;
SYSLINUX += mkdir -p $$(dir $$@) ;
SYSLINUX += tar -xz -C $$(dir $$@) -s '/.*/$$(notdir $$@)/'
SYSLINUX += -f $(SYSLINUX_TGZ)
SYSLINUX += syslinux-$(SYSLINUX_VERSION)/$$(SYSLINUX_$(1)_$(2))


# which files do we need to include to make syslinux work?
ifeq ($(SYSLINUX_VERSION),4.07)
SYSLINUX_C32_FILES = menu.c32
else ifeq ($(SYSLINUX_VERSION),6.03)
SYSLINUX_C32_FILES = ldlinux.c32 libcom32.c32 libutil.c32 menu.c32
else
SYSLINUX_C32_FILES = ERROR_ERROR
endif

# download the .tar.gz file
$(SYSLINUX_TGZ):
	curl --create-dirs -o $@ $(SYSLINUX_MIRROR)/$(notdir $@)


# instructions how to extract single files from the SysLinux archive
# The `touch` overwrites the modification time of the extracted file
# to avoid re-extracting the file on every run.
#$(SYSLINUX)/%: $(SYSLINUX_TGZ)
#	mkdir -p $(dir $@)
#	tar -xz -C $(dir $@) -f $< \
#		syslinux-$(SYSLINUX_VERSION)/$(FILEPATH)
#	touch $@

## specify path in syslinux-*.tar.gz for extraction
#$(SYSLINUX)/memdisk:      FILEPATH=bios/memdisk/memdisk
#$(SYSLINUX)/isolinux.bin: FILEPATH=bios/core/isolinux.bin
#$(SYSLINUX)/ldlinux.c32:  FILEPATH=bios/com32/elflink/ldlinux/ldlinux.c32
#
#$(SYSLINUX)/libcom32.c32: FILEPATH=bios/com32/lib/libcom32.c32
#$(SYSLINUX)/libutil.c32:  FILEPATH=bios/com32/libutil/libutil.c32
#$(SYSLINUX)/menu.c32:     FILEPATH=bios/com32/menu/menu.c32
##$(SYSLINUX)/libcom32.c32: FILEPATH=efi64/com32/lib/libcom32.c32
##$(SYSLINUX)/libutil.c32:  FILEPATH=efi64/com32/libutil/libutil.c32
##$(SYSLINUX)/menu.c32:     FILEPATH=efi64/com32/menu/menu.c32
#
#$(SYSLINUX)/mbr.bin:      FILEPATH=bios/mbr/mbr.bin
#$(SYSLINUX)/bootx64.efi:  FILEPATH=


## specify path in syslinux-*.tar.gz for extraction
SYSLINUX_bios_memdisk       = bios/memdisk/memdisk
SYSLINUX_bios_isolinux.bin  = bios/core/isolinux.bin
SYSLINUX_bios_ldlinux.c32   = bios/com32/elflink/ldlinux/ldlinux.c32
SYSLINUX_bios_libcom32.c32  = bios/com32/lib/libcom32.c32
SYSLINUX_bios_libutil.c32   = bios/com32/libutil/libutil.c32
SYSLINUX_bios_menu.c32      = bios/com32/menu/menu.c32

SYSLINUX_efi64_syslinux.efi = efi64/efi/syslinux.efi
SYSLINUX_efi64_ldlinux.e64  = efi64/com32/elflink/ldlinux/ldlinux.e64
SYSLINUX_efi64_libcom32.c32 = efi64/com32/lib/libcom32.c32
SYSLINUX_efi64_libutil.c32  = efi64/com32/libutil/libutil.c32
SYSLINUX_efi64_menu.c32     = efi64/com32/menu/menu.c32

#
#$(SYSLINUX)/mbr.bin:      FILEPATH=bios/mbr/mbr.bin
#$(SYSLINUX)/bootx64.efi:  FILEPATH=


#syslinux.cfg local.mk: tombe.yaml generate-sysconfig.py
#	./generate-sysconfig.py


# ----------------------------------------------------------------------------
# Debian downloads
#
DEBIAN_NETBOOTDIR=main/installer-amd64/current/images/netboot/debian-installer/amd64

download/debian/stretch/linux:
	mkdir -p $(dir $@)
	curl -o $@ $(DEBIAN_MIRROR)/dists/stretch/$(DEBIAN_NETBOOTDIR)/linux

download/debian/stretch/initrd.gz:
	mkdir -p $(dir $@)
	curl -o $@ $(DEBIAN_MIRROR)/dists/stretch/$(DEBIAN_NETBOOTDIR)/initrd.gz


# ----------------------------------------------------------------------------
# CentOS downloads

download/centos/7/vmlinuz:
	mkdir -p $(dir $@)
	curl -o $@ $(CENTOS_MIRROR)/7/os/x86_64/images/pxeboot/vmlinuz

download/centos/7/initrd.img:
	mkdir -p $(dir $@)
	curl -o $@ $(CENTOS_MIRROR)/7/os/x86_64/images/pxeboot/initrd.img



# ----------------------------------------------------------------------------
# ToMBE USB stick
#

# SysLinux is not running on macOS, so we need to install it by hand.
# See:
# https://www.pyrosoft.co.uk/blog/2013/01/09/creating-a-bootable-usb-stick-from-osx/

USB=/Volumes/Tom

install-syslinux-macos-%: $(SYSLINUX)/mbr.bin
	diskutil umount $*
	sudo dd conv=notrunc bs=440 count=1 if=$< \
	  of=$$(diskutil info $* | awk '/Part of Whole/ {print "/dev/"$$4}')
	diskutil mount $*

usb:	$(USB)/efi/boot/bootx64.efi \
	$(patsubst %,$(USB)/efi/boot/%, \
	bootx64.efi ldlinux.e64 libcom32.c32 libutil.c32 menu.c32)

#$(USB)/efi/boot/ldlinux.c32 \
#	$(USB)/efi/boot/libcom32.c32 \
#	$(USB)/efi/boot/libutil.c32 \
#	$(USB)/efi/boot/menu.c32

$(USB)/efi/boot/bootx64.efi:  $(call SYSLINUX,efi64,syslinux.efi)
$(USB)/efi/boot/ldlinux.e64:  $(call SYSLINUX,efi64,ldlinux.e64)
$(USB)/efi/boot/libcom32.c32: $(call SYSLINUX,efi64,libcom32.c32)
$(USB)/efi/boot/libutil.c32:  $(call SYSLINUX,efi64,libutil.c32)
$(USB)/efi/boot/menu.c32:     $(call SYSLINUX,efi64,menu.c32)


#define SL =
#$(SYSLINUX_TGZ) ;
#echo Hey
#mkdir -p $(dir $@)
#tar -xz -C $(dir $@) -s '/.*/$(notdir $@)/' -f $< \
#	$(patsubst %.tar.gz,%,$(notdir $<))/$(1)
#touch $@
#endef


#mkdir -p $$(dir $$@) ; \
#tar -xz -C $$(dir $$@) -s '/.*/$$(notdir $$@)/' -f $$< \
#		$(patsubst %.tar.gz,%,$(2))/$(3)
#	touch $@
#endef




#$(call TGZEXTRACT,efi64/efi/syslinux.efi)

#$(foreach prog,$(PROGRAMS),$(eval $(call PROGRAM_template,$(prog))))



# ----------------------------------------------------------------------------
# ToMBE ISO file
#

build/iso/%: download/%
	mkdir -p $(dir $@)
	ln -f $< $@

build/iso/syslinux/%: $(SYSLINUX)/%
	mkdir -p $(dir $@)
	ln -f $< $@

build/iso/syslinux/memdisk:      FILEPATH=bios/memdisk/memdisk
build/iso/syslinux/isolinux.bin: FILEPATH=bios/core/isolinux.bin
build/iso/syslinux/ldlinux.c32:  FILEPATH=bios/com32/elflink/ldlinux/ldlinux.c32
build/iso/syslinux/libcom32.c32: FILEPATH=bios/com32/lib/libcom32.c32
build/iso/syslinux/libutil.c32:  FILEPATH=bios/com32/libutil/libutil.c32
build/iso/syslinux/menu.c32:     FILEPATH=bios/com32/menu/menu.c32


#tombe.iso: $(shell \
#	awk '/^\s*(KERNEL|LINUX|INITRD)/ {print $$2}' \
#	syslinux.cfg)

tombe.iso: 	Makefile \
		ipxe/ipxe.lkrn \
		syslinux.cfg.erb \
		$(SYSLINUX_FILES:%=build/iso/syslinux/%)

	mkdir -p build/iso/boot

	erb syslinux.cfg.erb > build/iso/syslinux.cfg

	mkisofs -o $@ -V ToMBE \
		-b syslinux/isolinux.bin -c boot/boot.cat \
		-no-emul-boot -boot-load-size 4 -boot-info-table \
		build/iso
#-find . \
#		-path './syslinux/*' -o \
#		-path './boot/*' -o \
#		-path './ipxe/*' -o \
#		-path './debian/*'


# ----------------------------------------------------------------------------
