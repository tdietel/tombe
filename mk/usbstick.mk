
#kingston_UUID=ACCC7E2C-11B8-3771-95DA-8498E03F5BF8

ifeq ($(shell uname), Darwin)
kingston_UUID=E2EFEC2E-E224-31A8-92BA-BEB351F6FE5A
kingston_VOLNAME=Tom
kingston_DEVICE=$(shell diskutil list $(kingston_UUID) | awk '/^\/dev\// {print $$1}')
kingston_MOUNTPOINT=/Volumes/$(kingston_VOLNAME)
else
kingston_UUID=4422-0912
kingston_VOLNAME=$(shell /sbin/blkid -lt UUID=$(kingston_UUID) -s LABEL -o value)
kingston_DEVICE=$(shell /sbin/blkid -U $(kingston_UUID))
kingston_MOUNTPOINT=/media/$(kingston_VOLNAME)
endif

kingston_SERIAL=408D5C15AFB8E261090CB92B
kingston_INFO="Kingston Traveller USB stick"

kingston_FILES += syslinux.cfg


lsusb: kingston-vagrant-find
attach: kingston-vagrant-attach
detach: kingston-vagrant-detach
mount: kingston-mount
umount: kingston-umount

scan:: kingston-scan

kingston: $(patsubst %,$(kingston_MOUNTPOINT)/%,$(kingston_FILES))
kingston: kingston-umount



define TARGET_template =
$(1): $(1)-files $(1)-finish

$(1)-finish: kingston-files $(1)-umount

$(1)-files: $(1)-mount

#$(1)-mount: img/.$(1).mount

$(1)-umount: img/.tombe.umount

umount: $(1)-umount
clean:: ;  rm -f img/.$(1).*
endef

$(eval $(call TARGET_template, kingston))

kingston-files: $(kingston_MOUNTPOINT)/syslinux.cfg

$(kingston_MOUNTPOINT)/syslinux.cfg: syslinux.cfg kingston-mount
	cp $< $@






%-info:
	@echo "$*_UUID=$($*_UUID)"
	@echo "$*_DEVICE=$($*_DEVICE)"
	@echo "$*_SERIAL=$($*_SERIAL)"
	@echo "$*_MOUNTPOINT=$($*_MOUNTPOINT)"


%-vagrant-find:
	VBoxManage list usbhost | grep -B 8 -A 2 $($*_SERIAL)

%-vagrant-attach: #%-umount
	VBoxManage controlvm $(VAGRANTVM) usbattach \
	 $(shell VBoxManage list usbhost | grep -B 8 -A 2 $($*_SERIAL) \
	| awk '/^UUID/ {print $$2}')

%-vagrant-detach:
	VBoxManage controlvm $(VAGRANTVM) usbdetach \
	 $(shell VBoxManage list usbhost | grep -B 8 -A 2 $($*_SERIAL) \
	| awk '/^UUID/ {print $$2}')

img/kingston.vmdk: #kingston-umount
	@mkdir -p $(dir $@)
	@printf "Using Kingston device: "
	@ls $(kingston_DEVICE)

	sudo chmod 666 $(kingston_DEVICE)
	if VBoxManage showmediuminfo $@ >/dev/null 2>&1 ; then \
	  echo "removing medium"; VBoxManage closemedium $@ ; \
	fi


	#diskutil umount $(kingston_DEVICE)
	VBoxManage internalcommands createrawvmdk -filename $@ \
	-rawdisk $(kingston_DEVICE)



# =======================================================================
# Common targets for all platforms
# Common targets/commands

#mount: kingston-mount

kingston-mount: img/.kingston.mount
kingston-umount: img/.kingston.umount


# =======================================================================
# macOS specific targets and commands
#
ifeq ($(shell uname), Darwin)
%-scan::
	@diskutil list $($*_UUID) 2>&1 >/dev/null \
	&& echo "   $*    --  $($*_INFO)" \
	|| true

%-mount:
	diskutil mount $($*_UUID)

%-umount:
	diskutil umount $($*_UUID) && rm -f img/.mount.$*


# =======================================================================
# Generic/Linux targets and commands
#
else

%-scan::
	@/sbin/blkid -U $($*_UUID) 2>&1 >/dev/null \
	&& echo "   $*    --  $($*_INFO)" \
	|| true

img/.%.mount:
	@$(SUDO) mkdir -p $($*_MOUNTPOINT)
	$(SUDO) mount -o umask=0000 $($*_DEVICE) $($*_MOUNTPOINT)
	@touch $@
	@rm -f img/.$*.umount

img/.%.umount:
	$(SUDO) umount $($*_DEVICE)
	@$(SUDO) rmdir $($*_MOUNTPOINT)
	@rm -f img/.$*.mount
	@touch $@

endif
